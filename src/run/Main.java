package run;

import config.Config;
import config.ConfigurationLoadFile;
import core.FinderObjectDependencyPool;
import core.FinderObjectPool;
import core.PathFinder;
import matrix.DependencyMatrix;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.log4j.Priority;
import reporter.Reporter;

import java.util.Date;

import static config.Config.MERGE_DIR;
import static utils.Utils.scriptCopy;

/**
 * Created by dtn on 5/19/2017.
 */
public class Main {
    public static void main(String[] args) throws Exception {
        PathFinder finder = new PathFinder();
//        MemberDependency map = finder.findDependencyForMember("MyClass", "process");
//        DependencyMatrix map = finder.findInClass("MyClass");
//        DependencyMatrix map = finder.findInClass("com.viettel.bankplus.payapp.business.lixi.LixiOpenBusiness");
//        DependencyMatrix map = finder.findInClass("MyClass");





        DependencyMatrix map = finder.matrixFinder(ConfigurationLoadFile.getInstance().getAllClassesToFind(MERGE_DIR));
        map.print(Priority.INFO);
        FinderObjectPool.getInstance().printAll(Priority.INFO);
        FinderObjectDependencyPool.getInstance().printAllProblems();
        Reporter.exportToFile(Config.getInstance().template(), "CodeFinderReport_" + DateFormatUtils.format(new Date(), "yyMMddHHmmss") + ".xlsx",
                map, FinderObjectDependencyPool.getInstance(), FinderObjectPool.getInstance());

        scriptCopy(MERGE_DIR, "./live", "./up");
    }
}
