package core;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import javassist.CtBehavior;
import javassist.CtClass;
import javassist.CtMember;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.apache.ws.security.util.StringUtil;

import java.util.Arrays;
import java.util.Collections;

/**
 * Created by dtn on 4/23/2017.
 */
public class FinderObject {
    /* All types of access to a class (Where the access comes from)*/

    private static Logger logger = Logger.getLogger(FinderObject.class);

    public enum ACCESS_TYPE {
        METHOD_CALL,
        CAST,
        CONSTRUCTOR_CALL,
        FIELD_ACCESS,
        INSTANCE_OF,
        NEW_ARRAY,
        NEW_EXPRESSION;
    }

//    static final String METHOD_CALL = "METHOD CALL";
//    static final String CAST = "CAST";
//    static final String CONSTRUCTOR_CALL = "SUPER_THIS";
//    static final String FIELD_ACCESS = "FIELD ACCESS";
//    static final String INSTANCE_OF = "INSTANCE OF";
//    static final String NEW_ARRAY = "NEW ARRAY";
//    static final String NEW_EXPRESSION = "NEW EXPRESSION";

    private Multimap<CtClass, Integer> where;
    private CtClass clazz;
    private ACCESS_TYPE accessType;
    private CtMember accessedMember;

    Multimap<CtClass, Integer> getLocations() {
        return where;
    }

    public static class Location {
        private String fileName;
        private Integer lineNumber;
        private String stackTrace;

        public Location(String fileName, int lineNo, String stackTrace) {
            this.fileName = fileName;
            this.lineNumber = lineNo;
            this.stackTrace = stackTrace;
        }

        public String getEnclosing() {
            return fileName;
        }

        public void setEnclosing(String enclosing) {
            this.fileName = enclosing;
        }

        public Integer getLineNumber() {
            return lineNumber;
        }

        public void setLineNumber(Integer lineNumber) {
            this.lineNumber = lineNumber;
        }

        @Override
        public String toString() {
            return "Location{" +
                    "enclosing=" + fileName +
                    ", lineNumber=" + lineNumber +
                    '}';
        }

        public String getStacktrace() {
            return stackTrace;
        }
    }

    FinderObject(CtClass clazz, ACCESS_TYPE accessType, CtMember accessedMember) {
        this.clazz = clazz;
        this.accessType = accessType;
        this.accessedMember = accessedMember;
        where = HashMultimap.create();
    }

    FinderObject(CtClass className, ACCESS_TYPE accessType) {
        this.clazz = className;
        this.accessType = accessType;
    }

    FinderObject() {
    }

    public CtClass getClazz() {
        return clazz;
    }

    void setClazz(CtClass clazz) {
        this.clazz = clazz;
    }

    public ACCESS_TYPE getAccessType() {
        return accessType;
    }

    void setAccessType(ACCESS_TYPE accessType) {
        this.accessType = accessType;
    }

    public CtMember getAccessedMember() {
        return accessedMember;
    }

    public void setAccessedMember(CtMember accessedMember) {
        this.accessedMember = accessedMember;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FinderObject that = (FinderObject) o;

        // class phai giong nhau
        if (clazz != null ? !clazz.equals(that.clazz) : that.clazz != null) return false;

        // neu la 3 loai nay thi k can quan tam den loai member
        if (isClassAccess()) {
            return accessType.equals(that.accessType);
        }

        // so sanh member
        return accessedMember != null ? accessedMember.equals(that.accessedMember) : that.accessedMember == null;
    }

    @Override
    public int hashCode() {
        return getHashCode(clazz, accessType, accessedMember);
    }

    public boolean isClassAccess() {
        return ACCESS_TYPE.CAST.equals(accessType) || ACCESS_TYPE.INSTANCE_OF.equals(accessType) || ACCESS_TYPE.NEW_ARRAY.equals(accessType);
    }

    public boolean isMethodOrConstructorAccess() {
        return !isClassAccess() && ACCESS_TYPE.FIELD_ACCESS != accessType;
    }

    @Override
    public String toString() {
        String className = clazz.getName();
        if (isClassAccess()) {
            return String.format("%-15s\t%s", accessType, className);
        }
        return String.format("%-15s\t%s.%s", accessType.toString(), className, StringUtils.remove(accessedMember.getName(), className)); //, where.toString());
//        return clazz + "." + accessedMember.getName() +  "[" + accessType.toString() + "]" + ": " + "[" + ;
    }

    void putLineNumber(CtClass clazz, Integer no) {
        where.put(clazz, no);
    }

    static int getHashCode(CtClass className, ACCESS_TYPE accessType, CtMember accessedMember) {
        int result = className != null ? className.hashCode() : 0;
        result = 31 * result + (accessType != null ? accessType.hashCode() : 0);
        if (!(ACCESS_TYPE.CAST.equals(accessType) || ACCESS_TYPE.INSTANCE_OF.equals(accessType) || ACCESS_TYPE.NEW_ARRAY.equals(accessType)))
            result = 31 * result + (accessedMember != null ? accessedMember.hashCode() : 0);
        return result;
    }

    public void toDeepString(Priority logLevel) {
        logger.log(logLevel, String.format("============ <MEMBER [%s]> ============", this));
        logger.log(logLevel, "\tLocations");
        where.asMap().forEach((ctClass, integers) ->
                logger.log(logLevel, String.format("\t\tClass [%s]: Lines [%s]", ctClass.getSimpleName(), Arrays.toString(integers.toArray()))));
        logger.log(logLevel, String.format("============ </MEMBER [%s]> ============", this));

    }

    public boolean isCtBehavior() {
        return accessType == ACCESS_TYPE.METHOD_CALL || accessType == ACCESS_TYPE.NEW_EXPRESSION;
    }

    public String getShortName() {
        String className = clazz.getName();
        if (isClassAccess()) {
            return String.format("%s[%s]", className, accessType);
        }
        String out;
        if (isCtBehavior()) {
            CtBehavior behavior = (CtBehavior) accessedMember;
            out = behavior.getLongName();
        }
        else {
            out = accessedMember.getName();
        }
        return String.format("%s", StringUtils.remove(out, className)); //, where.toString());
    }


}
