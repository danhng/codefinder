package core;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import config.Config;
import javassist.CtBehavior;
import matrix.MemberDependency;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

import static core.PathFinder.FIND_CONSTRUCTOR;
import static core.PathFinder.FIND_METHOD;


/**
 * Created by dtn on 5/17/2017.
 * <p>
 * Chua cac object
 */
public class FinderObjectDependencyPool {
    private LoadingCache<CtBehavior, MemberDependency> cache;
    private static Logger logger = Logger.getLogger(FinderObjectDependencyPool.class);
    private static FinderObjectDependencyPool ins;
    // <Object name, Where, What error>
    private Multimap<String, FinderObject.Location> problems;
    private Set<CtBehavior> cachingSet; // prevent recursion caching from LoadingCache

    public static FinderObjectDependencyPool getInstance() {
        if (ins == null) {
            ins = new FinderObjectDependencyPool();
        }
        return ins;
    }

    private FinderObjectDependencyPool() {
        cache = CacheBuilder.newBuilder().maximumSize(10000).build(new CacheLoader<CtBehavior, MemberDependency>() {
            public MemberDependency load(CtBehavior key) {
                logger.debug("Loading dependency pool: " + key.getLongName());
                return PathFinder.getInstance()._findDependencyForMember(key, 0, Config.getInstance().getMaxNestLevel(), Config.getInstance().isFindKeyWordOnly(), Config.getInstance().getKeywords(), FIND_CONSTRUCTOR | FIND_METHOD);
            }
        });
        cachingSet = new HashSet<>();
        problems = HashMultimap.create();
    }

    public MemberDependency get(CtBehavior member) {
        MemberDependency memberDependency = cache.getIfPresent(member);
        if (memberDependency != null) {
            logger.debug("!!!!!!!!!!!!!!!!! CACHE HIT AT: " + member.getLongName());
            cachingSet.remove(member);
            return memberDependency;
        }
        logger.debug("XXXXXXXXXXXXXXXXX CACHE NOT HIT AT: " + member.getLongName());
        try {
            cachingSet.add(member);
            return cache.get(member);
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Set<CtBehavior> getCachingSet() {
        return cachingSet;
    }

    public void setCachingSet(Set<CtBehavior> cachingSet) {
        this.cachingSet = cachingSet;
    }

    public void recordProblem(String fileName, int lineNo, String errorDesc, String stackTrace) {
        FinderObject.Location location = new FinderObject.Location(fileName, lineNo, stackTrace);
        problems.put(errorDesc, location);
    }

    public void printAllProblems() {
        AtomicInteger counter = new AtomicInteger(0);
        logger.info(" PROBLEMS: ");
        if (problems.isEmpty()) {
            logger.info("Khong co van de nao ve thieu classfile, thieu method duoc tim thay,");
        }
        problems.asMap().forEach((errorDesc, locations) -> {
            logger.info(String.format("<%s>", counter.incrementAndGet()));
            logger.info(errorDesc);
            locations.forEach(location ->
            {
                logger.info(String.format("\t\t|_ %s.%s", location.getEnclosing(), location.getLineNumber()));
            });
            logger.info("--------------------------------------------------------");
//            logger.debug(String.format("%s [%s.%s]: %s", problem.getColumnKey(),
//                    location.getEnclosing(), location.getLineNumber(), problem.getValue()));
        });
    }

    public void clear() {
        cachingSet.clear();
        cache.cleanUp();
        problems.clear();
    }

    public Multimap<String, FinderObject.Location> getProblems() {
        return problems;
    }

    public void setProblems(Multimap<String, FinderObject.Location> problems) {
        this.problems = problems;
    }
}
