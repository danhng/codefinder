package core;

import com.viettel.bankplus.ussd.db.DBProcessor;
import javassist.*;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author thanhnd44 170508
 *         Viettel 2017 Confidential and Proprietary
 *         Created on 17/05/17
 */
public class FinderObjectPool {
    private static Logger logger = Logger.getLogger(FinderObjectPool.class);
    private ConcurrentMap<Integer, core.FinderObject> pool;

    private FinderObjectPool() {
        pool = new ConcurrentHashMap<>();
    }

    private static FinderObjectPool instance;

    public static FinderObjectPool getInstance() {
        if (instance == null) {
            instance = new FinderObjectPool();
        }
        return instance;
    }

    public FinderObject get(CtBehavior behavior) {
        FinderObject.ACCESS_TYPE access_type = behavior instanceof CtMethod ? FinderObject.ACCESS_TYPE.METHOD_CALL : FinderObject.ACCESS_TYPE.NEW_EXPRESSION;
        CtClass clazz = behavior.getDeclaringClass();
        int hashCode = core.FinderObject.getHashCode(clazz, access_type, behavior);
        FinderObject fo = new FinderObject(clazz, access_type, behavior);
        pool.putIfAbsent(hashCode, fo);
        FinderObject out = pool.get(hashCode);
        logger.debug(String.format("getting hashcode: (%s): (%s)", hashCode, String.valueOf(out)));
        return out;
    }

    public FinderObject get(CtClass className, core.FinderObject.ACCESS_TYPE access_type, CtMember member) {
        int hashCode = core.FinderObject.getHashCode(className, access_type, member);
        FinderObject fo = new FinderObject(className, access_type, member);
        pool.putIfAbsent(hashCode, fo);
        FinderObject out = pool.get(hashCode);
        logger.debug(String.format("getting hashcode: (%s): (%s)", hashCode, String.valueOf(out)));
        return out;
    }

    public void printAll(Priority logLevel) {
        pool.values().forEach(object -> object.toDeepString(logLevel));
        logger.info("TOTAL FINDER OBJECTS: " + pool.size());
    }

}