package core;

import javassist.CtClass;

import static core.FinderComparator.CompareStatus.A_B_COEXIST_SAME;

/**
 * @author thanhnd44 170508
 *         Viettel 2017 Confidential and Proprietary
 *         Created on 17/05/23
 * A comparator that compares class files
 * Goal: Find difference in every single member.
 */
public class FinderComparator {
    /**
     * Kiem tra xem bytecode co giong nhau o 2 method hay khong
     * 0: khong ton tai o ca 2
     * 1: ton tai o a, khong ton tai o b
     * 2: khong ton tai o a, ton tai o b
     * 3: ton tai o ca a va b, byte code khac nhau
     * 4: ton tai o ca a va b, byte code giong nhau
     *
     **/

    public enum CompareStatus {
        A_NOT_EXIST_B_NOT_EXIST,
        A_EXIST_B_NOT_EXIST,
        A_NOT_EXIST_B_EXIST,
        A_B_COEXIST_DIFFERENT,
        A_B_COEXIST_SAME;
    }

    private CompareStatus result;
    private String cpA;
    private String cpB;
    private String memberName;
    private String className;
    private String desc;

    public FinderComparator(CompareStatus result, String cpA, String cpB, String className, String methodName, String desc) {
        this.result = result;
        this.cpA = cpA;
        this.cpB = cpB;
        this.className = className;
        this.memberName = methodName;
        this.desc = desc;
    }

    public CompareStatus getResult() {
        return result;
    }

    public void setResult(CompareStatus result) {
        this.result = result;
    }

    public String getCpA() {
        return cpA;
    }

    public void setCpA(String cpA) {
        this.cpA = cpA;
    }

    public String getCpB() {
        return cpB;
    }

    public void setCpB(String cpB) {
        this.cpB = cpB;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public boolean isDifferent() {
        return result != A_B_COEXIST_SAME;
    }

    @Override
    public String toString() {
        return "FinderComparator{" +
                "result=" + result +
                ", cpA='" + cpA + '\'' +
                ", cpB='" + cpB + '\'' +
                ", memberName='" + memberName + '\'' +
                ", className='" + className + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FinderComparator that = (FinderComparator) o;
//        if (!(getCpA().equals(that.getCpA()) || getCpB().equals(that.getCpB())) ||
//                (getCpA().equals(that.getCpB()) || getCpB().equals(that.getCpA())))
//        return false;
        if (!getMemberName().equals(that.getMemberName())) return false;
        if (!getClassName().equals(that.getClassName())) return false;
        return getDesc().equals(that.getDesc());
    }

    @Override
    public int hashCode() {
        int result = getMemberName().hashCode();
        result = 31 * result + getClassName().hashCode();
        result = 31 * result + getDesc().hashCode();
        return result;
    }
}
