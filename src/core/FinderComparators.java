package core;

import javassist.CannotCompileException;
import javassist.CtBehavior;
import javassist.CtClass;
import org.apache.log4j.Logger;
import utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static utils.Utils._getClass;

/**
 * @author thanhnd44 170508
 *         Viettel 2017 Confidential and Proprietary
 *         Created on 17/05/23
 */
public class FinderComparators {

    private static Logger logger = Logger.getLogger(FinderComparators.class);

    public static List<FinderComparator> compare(String className, String classPathA, String classPathB) {
        CtClass a = _getClass(className, true, classPathA);
        CtClass b = _getClass(className, true, classPathB);

        if (a == null || b == null) {
            return null;
        }
        if (!a.getName().equals(b.getName())) {
            return null;
        }
        logger.info(String.format("Comparing class [%s] in [%s] and [%s]", a.getName(), classPathA, classPathB));

        List<CtBehavior> bevA = Utils.getDeclaredMembers(a);
        List<CtBehavior> bevB = Utils.getDeclaredMembers(b);

        // compared behavior so far
        List<String> compared = new ArrayList<>();
        List<FinderComparator> comparators = new ArrayList<>();

        bevA.forEach(behavior -> {
            try {
                logger.info("Comparing " + behavior.getLongName());
                FinderComparator.CompareStatus result = Utils.compareMemberByteCodes(a.getName(), behavior.getName(), behavior.getSignature(), classPathA, classPathB);
                comparators.add(new FinderComparator(result, classPathA, classPathB, a.getName(), behavior.getName(), behavior.getSignature()));
                compared.add(behavior.getName() + behavior.getSignature());
            } catch (CannotCompileException e) {
                e.printStackTrace();
            }
        });

        bevB.forEach(behavior -> {
            try {
                // neu chua ton tai thi so sanh
                if (!compared.contains(behavior.getName() + behavior.getSignature())) {
                    logger.info("Comparing B " + behavior.getLongName());
                    FinderComparator.CompareStatus result = Utils.compareMemberByteCodes(a.getName(), behavior.getName(), behavior.getSignature(), classPathA, classPathB);
                    comparators.add(new FinderComparator(result, classPathA, classPathB, a.getName(), behavior.getName(), behavior.getSignature()));
                    compared.add(behavior.getName() + behavior.getSignature());
                }
            } catch (CannotCompileException e) {
                e.printStackTrace();
            }
        });
        return comparators;
    }
}
