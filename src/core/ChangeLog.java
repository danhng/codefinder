package core;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * @author thanhnd44 170508
 *         Viettel 2017 Confidential and Proprietary
 *         Created on 17/05/23
 *
 * Đối với tất cả các method THAY ĐỔI, luôn luôn đặt Annotation này.
 */
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.METHOD, ElementType.CONSTRUCTOR})
public @interface ChangeLog {
    String author() default "Annonymous";
    String note() default "N/A";
}
