package core;

import com.google.common.collect.Multimap;
import javassist.*;
import javassist.expr.*;

import matrix.ClassDependency;
import matrix.DependencyMatrix;
import matrix.MemberDependency;

import static org.apache.commons.lang3.StringUtils.contains;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.apache.log4j.PropertyConfigurator;
import utils.Utils;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.indexOfAny;
import static utils.Utils.getRouteMessage;
import static utils.Utils.reportWhere;

/**
 * @author thanhnd44 170329
 *         Viettel 2017 Confidential and Proprietary.
 */
public final class PathFinder {
    private static Logger logger = Logger.getLogger(PathFinder.class);
    //    private final HashMap<String, Set<core.FinderObject>> BASE = new HashMap<>();
    private Properties properties = new Properties();
    private int maxNestLevel;
    private String[] keywords;
    private boolean findKeyOnly;

    private static final int INITIAL_NEST_LEVEL = 0;

    static final int FIND_METHOD = 1;
    static final int FIND_CONSTRUCTOR = 2;

    static {
        PropertyConfigurator.configure("../etc/log4j.cfg");
    }

    public PathFinder() {
        loadConfiguration();
        keywords = keywords();
        maxNestLevel = nestLevel();
        findKeyOnly = findKeyOnly();
    }

    public DependencyMatrix matrixFinder(String[] classes) throws Exception {
        Arrays.sort(classes);
        logger.info("Classes to find: ");
        Arrays.stream(classes).forEach(logger::info);
        DependencyMatrix matrix = DependencyMatrix.newMatrix(classes);
        long a = System.currentTimeMillis();
        AtomicInteger integer = new AtomicInteger(0);
        int total = classes.length;
        Arrays.stream(classes).forEach(s ->
        {
            try {
                if (matrix.getBack().containsKey(Utils.getMergeClass(s, false))) {
                    logger.warn("Already in matrix: " + s);
                }
                logger.info("Started finding dependency for class " + s);
                logger.debug("CLASS CLASS CLASS  CLASS CLASS CLASS  CLASS CLASS CLASS  CLASS CLASS CLASS  CLASS CLASS CLASS  CLASS CLASS CLASS  CLASS CLASS CLASS  CLASS CLASS CLASS  CLASS CLASS CLASS  CLASS CLASS CLASS ");
                DependencyMatrix classMatrix = findInClass(s);
                logger.debug("XXXXX XXXXX XXXXX  XXXXX XXXXX XXXXX  XXXXX XXXXX XXXXX  XXXXX XXXXX XXXXX  XXXXX XXXXX XXXXX  XXXXX XXXXX XXXXX  XXXXX XXXXX XXXXX  XXXXX XXXXX XXXXX  XXXXX XXXXX XXXXX  XXXXX XXXXX XXXXX ");
                matrix.merge(classMatrix);
                logger.info("Finished finding dependency for class " + s + String.format(" : %s/%s", integer.incrementAndGet(), total));
            } catch (Exception e) {
                logger.info("Co loi xay ra: " + getRouteMessage(e));
                e.printStackTrace();
                recordProblem(null, e, ExceptionUtils.getStackTrace(e));
            }
        });
        long b = System.currentTimeMillis();
        logger.info("Total classes: Took: " + (b - a) / 1000 + " seconds");
        return matrix;
    }

    /**
     * @param fullClassName full class name including package name
     * @return a map that maps fully qualified class name to its used member
     * @throws Exception the exception
     */
    private DependencyMatrix findInClass(String fullClassName) throws Exception {
        long before = System.currentTimeMillis();
        CtClass clazz = Utils.getMergeClass(fullClassName, true);

        if (clazz == null) {
            logger.debug("Cannot find class: " + fullClassName);
            return null;
        }

        List<CtBehavior> all = Utils.getUsedMembers(fullClassName);

        logger.debug("Cac method cua class " + fullClassName);
        all.forEach(t -> logger.debug(t.getLongName()));

        // new dependency matrix
        DependencyMatrix matrix = DependencyMatrix.newMatrix(new String[]{fullClassName});

        // ensure has class
        matrix.ensureHasClass(clazz);
        ClassDependency classDependency = matrix.getBack().get(clazz);

        // for each member dependency got, add it to class dependency
        all.forEach(ctBehaviour ->
        {
            logger.debug("START START START  START START START  START START START  START START START  START START START  START START START  START START START  START START START  START START START  START START START ");
            logger.debug("*********** Started add member dependency for: " + ctBehaviour.getLongName());
            MemberDependency memberDependency;
            memberDependency = findDependencyForMember(ctBehaviour);
            if (memberDependency != null) {
                classDependency.getBack().put(ctBehaviour, memberDependency);

                logger.debug("***********Finsished add member dependency for: " + ctBehaviour.getLongName());
                logger.debug("***********RESULT: ");
                memberDependency.print(Priority.DEBUG);
                logger.debug(" END END END  END END END  END END END  END END END  END END END  END END END  END END END  END END END  END END END  END END END");
            } else {
                logger.debug("Co loi xay ra khi tim kiem member: " + ctBehaviour.getLongName());
            }
        });

        logger.debug("FINALE: " + fullClassName);
        classDependency.print(Priority.DEBUG);
        long after = System.currentTimeMillis();
        logger.debug(String.format("Took (%s) seconds. Found (%s) dependency classes. \n\n", (after - before) / 1000, classDependency.getBack().size()));
        return matrix;
    }

//    private Set<core.FinderObject> findDependencyForMember(CtBehavior ctBehaviour) {
//    }


    /**
     * @return a map that maps fully qualified class name to its used member
     * @throws Exception the exception
     */
    private MemberDependency findDependencyForMember(String className, String methodName) {
        try {
            CtClass clazz = Utils.getMergeClass(className, true);
            return findDependencyForMember(clazz.getDeclaredMethod(methodName));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
//
//    /**
//     * @return a map that maps fully qualified class name to its used member
//     * @throws Exception the exception
//     */
//    private MemberDependency findDependencyForMember(String className, String methodName, String sign) {
//        try {
//            CtClass clazz = Utils.getMergeClass(className);
//            return findDependencyForMember(clazz.getMethod(methodName, sign));
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }

    /**
     * @return a map that maps fully qualified class name to its used member
     * @throws Exception the exception
     */
    MemberDependency findDependencyForMember(CtBehavior behavior) {
        try {
//            MemberDependency base = new MemberDependency(behavior);
            return _findDependencyForMember(behavior, 0, maxNestLevel, findKeyOnly, keywords, FIND_CONSTRUCTOR | FIND_METHOD);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

//    /**
//     * @param targetMember  method within the class
//     * @param maxNestLevel  max nest level
//     * @param findKeyOnly   whether to find the key based on @{keywords} only
//     * @param keywords      a list of keywords deciding whether or not to approve match
//     * @param findAs        find as method or constructor see @{@link core.PathFinder}
//     * @return a map that maps fully qualified class name to its used member
//     * @throws Exception the exception
//     */
//    private MemberDependency _findDependencyForMember(CtBehavior targetMember, int maxNestLevel, boolean findKeyOnly, String[] keywords, int findAs) throws Exception {
//        logger.debug("Clearing BASE");
//        BASE.clear();
//        _findDependencyForMemberInner(targetMember, INITIAL_NEST_LEVEL, maxNestLevel, findKeyOnly, keywords, findAs);
//        logger.debug(" ++++++++++++++++++++++++++++  <Final Base>  ++++++++++++++++++++++++++++ ");
//        print(BASE);
//        logger.debug(" ++++++++++++++++++++++++++++  </Final Base> ++++++++++++++++++++++++++++ ");
//
//    }

    private void _findDependencyForMember(CtBehavior targetMember, int initialNestLelvel, int maxNestLevel, boolean findKeyOnly, String[] keywords, int findAs, MemberDependency base) throws Exception {
        logger.debug("\n***************************************************************************************");
        logger.debug(format("FINDING FOR member [%s], NESTED LEVEL (%s), MAX NESTED LEVEL: (%s)", targetMember.getName(), initialNestLelvel, maxNestLevel));

        if (findKeyOnly && indexOfAny(targetMember.getLongName(), keywords) == -1) {
            logger.debug(String.format("(%s): nothing to do -> stop", targetMember.getLongName()));
            return;
        }

        if (initialNestLelvel > maxNestLevel) {
            logger.debug("REACHED MAX NESTED LEVEL -> STOP");
            return;
        }

        logger.debug("PRE BASE: ");
        base.print(Priority.DEBUG);
//        intercept(method, found, findKeyOnly, keywords);

        MemberDependency memberDependency = new MemberDependency(targetMember);
        intercept(targetMember, memberDependency, findKeyOnly, keywords);

        if (memberDependency.isEmpty()) {
            logger.debug("      [X] EMPTY. Returning at  " + targetMember.getLongName());
            logger.debug("BASE WHEN RETURN: ");
            base.print(Priority.DEBUG);
            return;
        }
        logger.debug(format("FINISHED FINDING FOR member (%s), NESTED LEVEL (%s) ", targetMember.getName(), initialNestLelvel));
        logger.debug("LOCAL IS:");
        memberDependency.print(Priority.DEBUG);

        logger.debug(format("[FINDING FOR member (%s), NESTED LEVEL (%s)]:  Merging found (size (%s)) into base...", targetMember.getLongName(), initialNestLelvel, memberDependency.getAllMemberDependency().size()));
        base.merge(memberDependency);
        logger.debug("AFTER MERGING... BASE IS:");
        base.print(Priority.DEBUG);

        for (FinderObject object : memberDependency.getAllMemberDependency()) {
            if (object.isMethodOrConstructorAccess())
                // access member (CtMember should be an instance of CtBahaviour if we get to this point
                _findDependencyForMember((CtBehavior) object.getAccessedMember(), initialNestLelvel + 1, maxNestLevel, findKeyOnly, keywords, FIND_METHOD | FIND_CONSTRUCTOR, base);
        }
    }

    MemberDependency _findDependencyForMember(CtBehavior targetMember, int initialNestLelvel, int maxNestLevel, boolean findKeyOnly, String[] keywords, int findAs) {
        logger.debug("\n***************************************************************************************");
        logger.debug(format("FINDING FOR member [%s], NESTED LEVEL (%s), MAX NESTED LEVEL: (%s)", targetMember.getName(), initialNestLelvel, maxNestLevel));

        MemberDependency memberDependency = new MemberDependency(targetMember);

        if (findKeyOnly && indexOfAny(targetMember.getLongName(), keywords) == -1) {
            logger.debug(String.format("(%s): nothing to do -> stop + remove caching set", targetMember.getLongName()));
            FinderObjectDependencyPool.getInstance().getCachingSet().remove(targetMember);
            return memberDependency;
        }

        if (initialNestLelvel > maxNestLevel) {
            logger.debug("REACHED MAX NESTED LEVEL -> STOP");
            return memberDependency;
        }

//        logger.debug("PRE BASE: ");
//        base.print();
//        intercept(method, found, findKeyOnly, keywords);

//        MemberDependency memberDependency = new MemberDependency(targetMember);
        intercept(targetMember, memberDependency, findKeyOnly, keywords);

        if (memberDependency.isEmpty()) {
            logger.debug("      [X] EMPTY. Returning Empty and remove caching set " + targetMember.getLongName());
            FinderObjectDependencyPool.getInstance().getCachingSet().remove(targetMember);
            return memberDependency;
        }
        logger.debug(format("FINISHED FINDING FOR member (%s), NESTED LEVEL (%s) ", targetMember.getName(), initialNestLelvel));
        logger.debug("LOCAL IS:");
        memberDependency.print(Priority.DEBUG);

        logger.debug(format("[FINDING FOR member (%s), NESTED LEVEL (%s)]:  Performing recursion", targetMember.getLongName(), initialNestLelvel));
//        base.merge(memberDependency);
//        logger.debug("AFTER MERGING... BASE IS:");
//        base.print();

        for (FinderObject object : memberDependency.getAllMemberDependency()) {
            if (object.isMethodOrConstructorAccess()) {
                // access member (CtMember should be an instance of CtBahaviour if we get to this point
                CtBehavior behavior = (CtBehavior) object.getAccessedMember();
                MemberDependency in = new MemberDependency(object);
                if (FinderObjectDependencyPool.getInstance().getCachingSet().contains(behavior)) {
                    logger.debug(String.format("Member [%s] is being cached. Don't recurse it.", behavior.getLongName()));
                } else {
                    logger.debug(String.format("Member [%s] is not being cached -> Loading...", behavior.getLongName()));
                    in = FinderObjectDependencyPool.getInstance().get(behavior);
                }
                logger.debug(String.format("Final dependency for Member [%s]: ", object.toString()));
                in.print(Priority.DEBUG);
                logger.debug(String.format("Merging MemberDependency of [%s]: ", object.toString()));
                memberDependency.merge(in);
//                        _findDependencyForMember((CtBehavior) object.getAccessedMember(), initialNestLelvel + 1, maxNestLevel, findKeyOnly, keywords, FIND_METHOD | FIND_CONSTRUCTOR));
            }
        }
        return memberDependency;
    }

    private void merge(HashMap<String, Set<core.FinderObject>> a, HashMap<String, Set<core.FinderObject>> b) {
        for (Map.Entry<String, Set<core.FinderObject>> bEntry : b.entrySet()) {
            logger.debug("MERGING FOR CLASS : " + bEntry.getKey());
            if (a.containsKey(bEntry.getKey())) {
                logger.debug("Map a Contains class already: " + bEntry.getKey());
                for (core.FinderObject m : bEntry.getValue()) {
                    if (a.get(bEntry.getKey()).contains(m)) {
                        logger.debug("Contains core.FinderObject already: " + m.toString());
                    } else {
                        logger.debug("Not Contains core.FinderObject. Adding: " + m);
                        a.get(bEntry.getKey()).add(m);
                    }
                }
            } else {
                logger.debug("Map a does not have class. Adding an entry for class: " + bEntry.getKey());
                a.put(bEntry.getKey(), bEntry.getValue());
            }
        }
    }

    private static void print(MemberDependency map) {
        int i = 0;
        if (map.isEmpty()) {
            logger.debug("[EMPTY MAP PRINT]");
        }
        logger.debug("=**********************************<" + map.getScopeName() + ">**********************************<");
        for (Map.Entry<CtClass, Collection<FinderObject>> a : map.getBack().asMap().entrySet()) {
            logger.debug("===============================<" + i + ">=====================================");
            String classN = a.getKey().getName();
            if (classN.matches("(.*)(viettel|bankplus)(.*)")) {
                logger.debug("BANKPLUS CLASS");
            }
            logger.debug("Class " + a.getKey());
            logger.debug("-----------------------------------------------------------------------");
            logger.debug("Cac ham da goi:");
            for (FinderObject b : a.getValue()) {
                if (b.isClassAccess()) {
                    logger.debug(format("-> [%s]", b.getAccessType()));
                } else {
                    logger.debug(format("-> [%s]: %s(%s)", b.getAccessType(), b.getAccessedMember().getName(), b.getAccessedMember().getSignature()));
                }
            }
            logger.debug("===============================</" + i++ + ">====================================\n------");
        }
    }

//    private void ensureBASEhasClass(CtClass clazz, MemberDependency dependency) {
//        dependency.(clazz, new HashSet<>());
//    }

    private void addFinderObject(MemberDependency found, core.FinderObject finderObject, boolean findKeyOnly, String[] keywords) {
        _addFinderObject(found, finderObject, findKeyOnly, keywords);
    }

    private void _addFinderObject(MemberDependency found, core.FinderObject finderObject, boolean findKeyOnly, String[] keywords) {
        logger.debug("Adding finder object: " + finderObject);
        String className = finderObject.getClazz().getName();
        String scopeName = found.getScopeName();
        String logName = "MemberDependency for " + scopeName;
        Multimap<CtClass, FinderObject> map = found.getBack();
        CtClass clazz = finderObject.getClazz();

        if (findKeyOnly && indexOfAny(className, keywords) == -1) {
            logger.debug(format("[OUTSIDER] Finder Object -> STOP [%s]", finderObject));
            return;
        }
        logger.debug(format("    -> Testing if core.FinderObject is in BASE yet: core.FinderObject (%s)", finderObject));
//        ensureBASEhasClass(className);
        if (!map.containsValue(finderObject)) {
            logger.debug(format("        [METHOD DEPENDENCY] [O] (%s) does not contain core.FinderObject (%s). PUT IT TO LOCAL MAP FOR RECURSION!!", logName, finderObject));
            map.put(clazz, finderObject);
//                                logger.debug(String.format("        [BASE] [O] Entry of class (%s) does not contain method (%s). PUT IT TO LOCAL MAP FOR RECURSION!!", className, method1.getLongName()));
        } else {
            logger.debug(format("       [METHOD DEPENDENCY] [X] Already contained FinderObject (%s). DO NOT PUT IT TO LOCAL MAP HERE!", finderObject));
        }
    }

    private void loadConfiguration() {
        try (InputStream stream = new FileInputStream("../etc/config.properties")) {
            properties.load(stream);
//            logger.debug("Loaded configuration...");
            properties.list(System.out);
        } catch (Exception e) {
            e.printStackTrace();
//            logger.error(e);
        }

    }

    private int nestLevel() {
        return Integer.parseInt(properties.getProperty("nest_level"));
    }

    private String[] keywords() {
        return properties.getProperty("keyword").split(",");
    }

    private boolean findKeyOnly() {
        return "1".equals(properties.getProperty("find_keyword_pkg_only"));
    }


    private void intercept(CtBehavior behavior, MemberDependency found, boolean findKeyOnly, String[] keywords) {
// todo add constructor
        try {
            behavior.instrument(
                    new ExprEditor() {
                        public void edit(MethodCall m)
                                throws CannotCompileException {
                            reportWhere(m);
                            CtMethod method1 = null;
                            String className = m.getClassName();
                            try {
                                method1 = m.getMethod();
                            } catch (NotFoundException e) {
//                                e.printStackTrace();
                                recordProblem(m, e, ExceptionUtils.getStackTrace(e));
                            }
                            if (method1 != null) {
                                FinderObject fo = FinderObjectPool.getInstance().get(method1.getDeclaringClass(), FinderObject.ACCESS_TYPE.METHOD_CALL, method1);
                                fo.getLocations().put(m.getEnclosingClass(), m.getLineNumber());
                                addFinderObject(found, fo, findKeyOnly, keywords);
                            }
                        }

                        @Override
                        public void edit(NewExpr e) throws CannotCompileException {
                            CtConstructor constructor = null;
                            reportWhere(e);
                            try {
                                constructor = e.getConstructor();
                            } catch (NotFoundException e1) {
//                                e1.printStackTrace();
                                recordProblem(e, e1, ExceptionUtils.getStackTrace(e1));
                            }
                            if (constructor != null) {
                                FinderObject fo = FinderObjectPool.getInstance().get(constructor.getDeclaringClass(), FinderObject.ACCESS_TYPE.NEW_EXPRESSION, constructor);
                                fo.getLocations().put(e.getEnclosingClass(), e.getLineNumber());
                                addFinderObject(found, fo, findKeyOnly, keywords);
                            }
                        }

                        @Override
                        public void edit(ConstructorCall c) throws CannotCompileException {
                            reportWhere(c);
                            CtConstructor constructor = null;
                            String className = c.getClassName();
                            try {
                                constructor = c.getConstructor();
                            } catch (NotFoundException e1) {
//                                e1.printStackTrace();
                                recordProblem(c, e1, ExceptionUtils.getStackTrace(e1));
                            }
                            if (constructor != null) {
                                FinderObject fo = FinderObjectPool.getInstance().get(constructor.getDeclaringClass(), FinderObject.ACCESS_TYPE.CONSTRUCTOR_CALL, constructor);
                                fo.getLocations().put(c.getEnclosingClass(), c.getLineNumber());
                                addFinderObject(found, fo, findKeyOnly, keywords);
                            }
                        }

                        @Override
                        public void edit(FieldAccess f) throws CannotCompileException {
                            reportWhere(f);
                            CtField field = null;
                            String className = f.getClassName();
                            try {
                                field = f.getField();
                            } catch (NotFoundException e1) {
//                                e1.printStackTrace();
                                recordProblem(f, e1, ExceptionUtils.getStackTrace(e1));
                            }
                            if (field != null) {
                                FinderObject fo = FinderObjectPool.getInstance().get(field.getDeclaringClass(), FinderObject.ACCESS_TYPE.FIELD_ACCESS, field);
                                fo.getLocations().put(f.getEnclosingClass(), f.getLineNumber());
                                addFinderObject(found, fo, findKeyOnly, keywords);
                            }
                        }

                        @Override
                        public void edit(Instanceof i) throws CannotCompileException {
                            reportWhere(i);
                            CtClass clazz = null;
                            try {
                                clazz = i.getType();
                            } catch (NotFoundException e) {
//                                e.printStackTrace();
                                recordProblem(i, e, ExceptionUtils.getStackTrace(e));
                            }
                            if (clazz != null) {
                                FinderObject fo = FinderObjectPool.getInstance().get(clazz, FinderObject.ACCESS_TYPE.INSTANCE_OF, null);
                                fo.getLocations().put(i.getEnclosingClass(), i.getLineNumber());
                                addFinderObject(found, fo, findKeyOnly, keywords);
                            }
                        }

                        @Override
                        public void edit(Cast c) throws CannotCompileException {
                            reportWhere(c);
                            CtClass clazz = null;
                            try {
                                clazz = c.getType();
                            } catch (NotFoundException e) {
//                                e.printStackTrace();
                                recordProblem(c, e, ExceptionUtils.getStackTrace(e));
                            }

                            if (clazz != null) {
                                FinderObject fo = FinderObjectPool.getInstance().get(clazz, FinderObject.ACCESS_TYPE.CAST, null);
                                fo.getLocations().put(c.getEnclosingClass(), c.getLineNumber());
                                addFinderObject(found, fo, findKeyOnly, keywords);
                            }
                        }

                        /**
                         * Edits an expression for array creation (overridable).
                         * The default implementation performs nothing.
                         *
                         * @param a the <tt>new</tt> expression for creating an array.
                         * @throws CannotCompileException
                         */
                        @Override
                        public void edit(NewArray a) throws CannotCompileException {
                            CtClass clazz = null;
                            try {
                                reportWhere(a);
                                clazz = a.getComponentType();
                            } catch (NotFoundException e) {
//                                e.printStackTrace();
                                recordProblem(a, e, ExceptionUtils.getStackTrace(e));
                            }
                            if (clazz != null) {
                                FinderObject fo = FinderObjectPool.getInstance().get(clazz, FinderObject.ACCESS_TYPE.NEW_ARRAY, null);
                                fo.getLocations().put(a.getEnclosingClass(), a.getLineNumber());
                                addFinderObject(found, fo, findKeyOnly, keywords);
                            }
                        }
                    });
        } catch (CannotCompileException e) {
            e.printStackTrace();
        }
    }

    private static PathFinder instance;

    public static PathFinder getInstance() {
        if (instance == null) {
            instance = new PathFinder();
        }
        return instance;
    }

    private void recordProblem(Expr a, Exception ex, String stackTrace) {
        String desc = getRouteMessage(ex);
        logger.warn("Record problem: " + desc);
        if (a != null) {
            FinderObjectDependencyPool.getInstance().recordProblem(a.getFileName(), a.getLineNumber(), desc, stackTrace);
        } else {
            FinderObjectDependencyPool.getInstance().recordProblem("N/A", 0, desc, stackTrace);
        }
    }
}
