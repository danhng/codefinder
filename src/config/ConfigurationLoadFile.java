/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import com.google.common.collect.Iterables;
import org.apache.log4j.Logger;
import utils.Utils;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static com.google.common.collect.Iterables.toArray;

/**
 *
 * @author HuyNQ25 Created on Dec 22, 2012
 */
public class ConfigurationLoadFile {

    private Logger log = Logger.getLogger(ConfigurationLoadFile.class.getSimpleName());
    private static ConfigurationLoadFile config;
    private Properties prop;
    // thanhnd44 170329 bccs gw ws
    private final boolean isLiveSystem = false;

    public static ConfigurationLoadFile getInstance() {
        if (config == null) {
            config = new ConfigurationLoadFile();
        }

        return config;
    }

    public ConfigurationLoadFile() {

        try {
            prop = new Properties();
            prop.load(new FileInputStream(Config.FileConfig.CONFIG_PARAM));
        } catch (Exception e) {
            log.info("Cannot load config file", e);

        }
    }

    public String getParam(String param) {
        return prop.getProperty(param);
    }

    public String[] getClassesToFindConfig() {
        int i = 0;
        List<String> classes = new ArrayList<>();
        while (true) {
            if (null == getParam("bs." + i + ".classname")) {
                break;
            }
            String bsClassName = (String) getParam("bs." + i + ".classname");
            classes.add(bsClassName);
            i++;
        }
        classes.forEach(s -> log.debug(s));
        return classes.toArray(new String[classes.size()]);
    }

    public String[] getAllClassesToFind(String dir) {
     List<String> string = Utils.listFiles(dir);
     return toArray(string, String.class);
    }
}
