package reporter;

import core.FinderObject;
import core.FinderObjectDependencyPool;
import core.FinderObjectPool;
import javassist.CtBehavior;
import matrix.ClassDependency;
import matrix.DependencyMatrix;
import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import utils.Utils;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static config.Config.UPCODE_DIR;
import static java.lang.String.*;

/**
 * Created by dtn on 5/19/2017.
 */
public class Reporter {

    private static Logger logger = Logger.getLogger(Reporter.class);
    private static int sheetCount = 0;

    public static void exportToFile(String templateFile, String fileName, DependencyMatrix matrix, FinderObjectDependencyPool problems, FinderObjectPool objectPool) throws IOException, InvalidFormatException {
//        Path templatePath = FileSystems.getDefault().getPath(templateFile);
//        Path reportPath = FileSystems.getDefault().getPath(fileName);
//        Files.copy(templatePath, reportPath, REPLACE_EXISTING);
        sheetCount = 0;
        Workbook workbook = WorkbookFactory.create(new File(templateFile));
        logger.info(format("Report [%s] has been initialised.", fileName));

        workbook.createSheet("a");
//        workbook.createSheet("a");

        logger.info("Importing problems...");
        importProblem(workbook, problems);
        logger.info("Imported problems successfully");

        logger.info("Importing dependency matrix...");
        importDependencyMatrix(workbook, matrix);
        logger.info("Imported dependency matrix successfully");

        // remove class template
        workbook.removeSheetAt(3);

        logger.info("Importing upcode review...");
        importUpcodeClass(workbook, Utils.listFiles(UPCODE_DIR),  matrix);
        logger.info("Imported upcode review");

        workbook.setActiveSheet(0);
        workbook.write(new FileOutputStream(fileName));

        logger.info(format("Saved to File [%s] at: [%s].", fileName, new Date()));
        logger.info(format("Opening [%s]....", fileName));
        Desktop dt = Desktop.getDesktop();
        dt.open(new File(fileName));
    }

    private static void importProblem(Workbook workbook, FinderObjectDependencyPool pool) {
        Sheet sheet = workbook.getSheet("Problems");
        AtomicInteger atomicInteger = new AtomicInteger(1);
        // import data
        pool.getProblems().asMap().forEach((s, locations) ->
                {
                    locations.forEach(location -> {
                        Row row = sheet.createRow(atomicInteger.getAndIncrement());
                        Cell predict = row.createCell(0);
                        Cell desc = row.createCell(1);
                        Cell file = row.createCell(2);
                        Cell lineNo = row.createCell(3);
                        Cell stackTrace = row.createCell(4);
                        desc.setCellValue(s);
                        file.setCellValue(location.getEnclosing());
                        lineNo.setCellValue(location.getLineNumber());
                        predict.setCellValue(getPrediction(s));
                        stackTrace.setCellValue(location.getStacktrace());
                    });
                }
        );
        sheet.autoSizeColumn(0);
        sheet.autoSizeColumn(1);
        sheet.autoSizeColumn(2);
    }

    private static void importDependencyMatrix(Workbook workbook, DependencyMatrix matrix) {
        AtomicInteger atomicInteger = new AtomicInteger(1);
        int total = matrix.getBack().size();
        java.util.List<ClassDependency> list = new ArrayList<>(matrix.getBack().values());
        Collections.sort(list);
        list.forEach(classDependency ->
        {
            logger.info(format("Importing dependency matrix %s [%s]/[%s]", classDependency.getClazz().getName(), atomicInteger.getAndIncrement(), total));
            importMemberDependency(workbook, classDependency);
        });
    }

    private static void importMemberDependency(Workbook workbook, ClassDependency classDependency) {
        Sheet sheet = workbook.cloneSheet(3);
        workbook.setSheetName(workbook.getSheetIndex(sheet), classDependency.getClazz().getSimpleName() + sheetCount++);
        AtomicInteger atomicInteger = new AtomicInteger(2);
        // ten class
        Row classRow = sheet.createRow(0);
        Cell cell = classRow.createCell(0);
        cell.setCellValue(classDependency.getClazz().getName());

        classDependency.getBack().forEach((ctBehavior, memberDependency) -> {
            memberDependency.getBack().values().forEach(finderObject -> {
                Row row = sheet.createRow(atomicInteger.getAndIncrement());
                Cell member = row.createCell(0);
                Cell dependencyClass = row.createCell(1);
                Cell dependencyMemberName = row.createCell(2);
                Cell accessType = row.createCell(3);
                member.setCellValue(ctBehavior.getName());
                dependencyClass.setCellValue(finderObject.getClazz().getName());
                dependencyMemberName.setCellValue(finderObject.getShortName());
                accessType.setCellValue(finderObject.getAccessType().toString());
            });
        });
        sheet.autoSizeColumn(0);
        sheet.autoSizeColumn(1);
        sheet.autoSizeColumn(2);
        sheet.autoSizeColumn(3);
    }

//    private static void importUpcodeClasses(Workbook workbook, List<String> upcodeClasses, DependencyMatrix matrix) {
//        upcodeClasses.forEach(s -> importUpcodeClass(workbook, s, matrix));
//    }

    private static void importUpcodeClass(Workbook workbook, List<String> upcodeClasses, DependencyMatrix matrix) {
        try {
            Sheet sheet = workbook.getSheet("Upcode Change");
            AtomicInteger atomicInteger = new AtomicInteger(2);
            upcodeClasses.forEach(s -> {
                        List<CtBehavior> behaviors = Utils.getDeclaredMembers(Utils.getMergeClass(s, true));
                        // ten class


                        behaviors.forEach((ctBehavior -> {
                            Row row = sheet.createRow(atomicInteger.getAndIncrement());

                            FinderObject object = FinderObjectPool.getInstance().get(ctBehavior);

                            Cell clazz = row.createCell(0);
                            Cell member = row.createCell(1);
                            Cell accessType = row.createCell(2);
                            Cell dependencyCount = row.createCell(3);

                            clazz.setCellValue(s);
                            member.setCellValue(object.getShortName());
                            accessType.setCellValue(object.getAccessType().toString());

                            AtomicInteger dependencyC = new AtomicInteger(0);
                            matrix.getBack().values().forEach(classDependency -> {
                                if (classDependency.dependOn(object)) {
                                    logger.info(format("[%s] has class that depends on it: [%s]", object, classDependency.getClazz().getName()));
                                    dependencyC.incrementAndGet();
                                }
                            });
                            dependencyCount.setCellValue(dependencyC.intValue());
                        }));
                    });
            sheet.autoSizeColumn(0);
//            sheet.autoSizeColumn(1);
            sheet.autoSizeColumn(2);
            sheet.autoSizeColumn(3);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String getPrediction(String err) {
        try {
            if (err.contains("is not found in")) {
                return "No Such Method";
            }
            if (err.contains("NotFoundException")) {
                return "Class Not Found";
            } else {
                return "Không chuẩn đoán được";
            }
        } catch (Exception e) {
            return "Không chuẩn đoán được";
        }
    }

}
