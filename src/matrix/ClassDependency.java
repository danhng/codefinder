package matrix;

import core.FinderObject;
import javassist.CtBehavior;
import javassist.CtClass;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author thanhnd44 170508
 *         Viettel 2017 Confidential and Proprietary
 *         Created on 17/05/17
 *         Represents all dependecies on which a class depends on
 */
public class ClassDependency implements Comparable<ClassDependency> {
    private Map<CtBehavior, MemberDependency> back;
    private CtClass clazz;

    private static Logger logger = Logger.getLogger(ClassDependency.class);

    public Map<CtBehavior, MemberDependency> getBack() {
        return back;
    }

    public void setBack(Map<CtBehavior, MemberDependency> back) {
        this.back = back;
    }

    public ClassDependency(CtClass clazz) {
        back = new HashMap<>();
        this.clazz = clazz;
    }

    /**
     * get all dependencies on this class
     * @return
     */
    public MemberDependency getAllDependencies() {
        MemberDependency out = new MemberDependency("Class " + clazz.getName());
        back.values().forEach(out::merge);
        return out;
    }

    public MemberDependency getMemberDependency(CtBehavior member) {
        return back.get(member);
    }

    public MemberDependency putIfAbsentMemberDependency(CtBehavior member, MemberDependency dependency) {
        return back.putIfAbsent(member, dependency);
    }

    public void print(Priority level) {
        back.keySet().forEach(
                member ->
                {
//                    logger.debug("\t\t\t" + member.getName());
                    getMemberDependency(member).print(level);
                }

        );
    }
    public boolean isEmpty() {
        return back.isEmpty();
    }

    public CtClass getClazz() {
        return clazz;
    }

    public void setClazz(CtClass clazz) {
        this.clazz = clazz;
    }

    @Override
    public int compareTo(ClassDependency o) {
        return clazz.getName().compareTo(o.getClazz().getName());
    }

    /**
     * class nay co phu thuoc vao ham tham so khong
     * @param finderObject
     * @return
     */
    public boolean dependOn(FinderObject finderObject) {
       return getAllDependencies().getBack().values().contains(finderObject);
    }
}
