package matrix;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import core.FinderObject;
import javassist.CtBehavior;
import javassist.CtClass;
import javassist.CtMember;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

import java.util.*;

/**
 * @author thanhnd44 170508
 *         Viettel 2017 Confidential and Proprietary
 *         Created on 17/05/16
 */
public class DependencyMatrix {
    private Map<CtClass, ClassDependency> back;
    private static Logger logger = Logger.getLogger(DependencyMatrix.class);
    private String[] classes;

    private DependencyMatrix(String[] classes) {
        back = new HashMap<>();
        this.classes = classes;
    }

    public String[] getClasses() {
        return classes;
    }

    public void setClasses(String[] classes) {
        this.classes = classes;
    }

    public static DependencyMatrix newMatrix(String[] classes) {
        return new DependencyMatrix(classes);
    }

    public Map<CtClass, ClassDependency> getBack() {
        return back;
    }

    public void setBack(Map<CtClass, ClassDependency> back) {
        this.back = back;
    }

    ClassDependency getClassDependency(CtClass clazz) {
        return back.get(clazz);
    }

//    Set<FinderObject> getFlattenedDependency(CtClass clazz) {
//        Multimap<CtBehavior, FinderObject> dependency = getDependency(clazz);
//        return new TreeSet<FinderObject>(dependency.values());
//    }

    void addDependency(CtClass d_clazz, CtBehavior d_dependant, FinderObject e_dependency) {
        back.putIfAbsent(d_clazz, new ClassDependency(d_clazz));
        ClassDependency classDependency = back.get(d_clazz);

        classDependency.putIfAbsentMemberDependency(d_dependant, new MemberDependency(d_dependant));
        MemberDependency memberDependency = classDependency.getMemberDependency(d_dependant);

        memberDependency.addDependency(e_dependency);
        logger.debug(String.format("    Class [%s]\n    Member [%s]\n     -> New dependency: [%s]\n", d_clazz, d_dependant, e_dependency));
    }

    public void print(Priority level) {
        back.forEach((ctClass, classDependency) ->
                {
                    logger.log(level, "\t\t Class dependency : " + ctClass.getName());
                    classDependency.print(level);
                }
        );
    }

    private String asClass(CtClass clazz) {
        return String.format("[%100s] \t\t Has", clazz.getName());
    }

//    private String asClassMemeber(CtBehavior object) {
//        return String.format(" | [%s]\t\t Depends on ", StringUtils.leftPad(" ", 50) + object.getName());
//    }
//
//    private String asClassMemeberDependency(FinderObject object) {
//        CtMember member = object.getAccessedMember();
//        return String.format(" | [%s]\t\t", StringUtils.leftPad(" ", 200) + (member != null ? member.getName() : "" +  "[" + object.getAccessType() + "]" ));
//    }

    public void clear() {
        back.clear();
    }

    public boolean isEmpty() {
        return back.isEmpty();
    }

    public boolean containsKey(CtClass ctClass) {
        return back.containsKey(ctClass);
    }

    public void ensureHasClass(CtClass ctClass) {
        back.putIfAbsent(ctClass, new ClassDependency(ctClass));
    }

    public void merge(DependencyMatrix that) {
        if (that != null) {
            that.back.forEach((key, value) -> back.put(key, value));
        }
    }
}
