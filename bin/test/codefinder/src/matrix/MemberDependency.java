package matrix;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import core.FinderObject;
import javassist.CtClass;
import javassist.CtMember;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

import java.util.HashSet;
import java.util.Set;

/**
 * @author thanhnd44 170508
 *         Viettel 2017 Confidential and Proprietary
 *         Created on 17/05/17
 */
public class MemberDependency {
    private Multimap<CtClass, FinderObject> back;
    private Object scope; // ctmember or class

    private static Logger logger = Logger.getLogger(MemberDependency.class);

    public MemberDependency(Object scope) {
        back = HashMultimap.create();
        this.scope = scope;
    }

    public Set<FinderObject> getAllMemberDependency() {
        return new HashSet<>(back.values());
    }

    public Set<FinderObject> getMemberDependency(CtClass clazz) {
        return new HashSet<>(back.get(clazz));
    }

    public void addDependency(FinderObject object) {
        back.put(object.getClazz(), object);
    }

    public void print(Priority level) {
        StringBuilder builder;
        logger.log(level, "\t\t\t |_ Member Dependency: " + getScopeName());
        if (back.keySet().isEmpty()) {
            logger.log(level, "\t\t\t | \t |_ EMPTY");
            return;
        }
        back.keySet().forEach(
                ctClass ->
                {
                    logger.log(level, "\t\t\t | \t |_ Class " + ctClass.getName());
                    getMemberDependency(ctClass).forEach(finderObject ->
                    {
                        logger.log(level, "\t\t\t | \t | \t |_ " + finderObject);
                    });
                }
        );
    }

    public Object getScope() {
        return scope;
    }

    public void setScope(Object scope) {
        this.scope = scope;
    }

    public void merge(MemberDependency that) {
//        MemberDependency neww = new MemberDependency();
        // copy this back into neww
//        back.entries().forEach(entry -> neww.back.put(entry.getKey(), entry.getValue()));
        that.back.entries().forEach(entry -> back.put(entry.getKey(), entry.getValue()));
    }

    public boolean isEmpty() {
        return back.isEmpty();
    }

    public Multimap<CtClass, FinderObject> getBack() {
        return back;
    }

    public void setBack(Multimap<CtClass, FinderObject> back) {
        this.back = back;
    }

    public String getScopeName() {
        if (scope instanceof CtMember) {
            CtMember scopeCt = (CtMember) scope;
            return scopeCt.getDeclaringClass().getName() + "." + scopeCt.getName() + "(" + scopeCt.getSignature() + ")";
        }
        if (scope instanceof CtClass) {
            CtClass scopeCt = (CtClass) scope;
            return scopeCt.getName();
        }
        return scope.toString();
    }
}
