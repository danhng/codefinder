package utils;

import com.google.common.collect.Lists;
import config.Config;
import core.*;
import javassist.*;
import javassist.expr.Expr;
import javassist.expr.ExprEditor;
import javassist.expr.MethodCall;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import static config.Config.LIVE_DIR;
import static config.Config.MERGE_DIR;
import static config.Config.UPCODE_DIR;
import static core.FinderComparator.CompareStatus.*;
import static org.apache.commons.lang3.StringUtils.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author thanhnd44 170508
 *         Viettel 2017 Confidential and Proprietary
 *         Created on 17/05/16
 */
public class Utils {

    private static Logger logger = Logger.getLogger(Utils.class);

    public static List<CtBehavior> getUsedMembers(String fullClassName) throws NotFoundException {
       return getUsedMembers(getMergeClass(fullClassName, true));
    }

    public static List<CtBehavior> getUsedMembers(CtClass clazz) throws NotFoundException {
        List<CtBehavior> out = getDeclaredMembers(clazz);
//        // 1. declared methods
//        CtBehavior[] declaredMethods = clazz.getDeclaredMethods();
        // 2. superclass methods
        List<CtBehavior> superClassesMethods = getSuperClassesMethods(clazz);
//        // 3. constructors
//        CtBehavior[] constructors = clazz.getDeclaredConstructors();
//        // 4. static block
//        CtBehavior initializer = clazz.getClassInitializer();
        out.addAll(superClassesMethods);
//        if (initializer != null) {
//            out.addAll(Lists.newArrayList(initializer));
//        }
        return out;
    }

    public static List<CtBehavior> getDeclaredMembers(CtClass clazz) {
          try {
            List<CtBehavior> out = new ArrayList<>();
            // 1. declared methods
            CtBehavior[] declaredMethods = clazz.getDeclaredMethods();
            // 2. constructors
            CtBehavior[] constructors = clazz.getDeclaredConstructors();
            // 3. static block
            CtBehavior initializer = clazz.getClassInitializer();
            out.addAll(Arrays.asList(constructors));
            out.addAll(Arrays.asList(declaredMethods));
            if (initializer != null) {
                out.addAll(Lists.newArrayList(initializer));
            }
            return out;
        }
        catch (Exception e) {
              e.printStackTrace();
              return new ArrayList<>();
        }
    }


    /**
     * Get all methods of super classes that are used by the given class.
     * @param clazz the given class
     * @return
     */
    private static List<CtBehavior> getSuperClassesMethods(CtClass clazz) throws NotFoundException {
        try {
            Config config = Config.getInstance();
            logger.debug("getSuperClassesMethods: " + clazz.getName());
            List<CtBehavior> ctMethods = new ArrayList<>();
            CtClass superClass = clazz.getSuperclass();
            String className = clazz.getName();
            String superClassName;
            if (superClass == null || !hasKeyWords(clazz) || !hasKeyWords(superClass)) {
                logger.debug(String.format(" [%s] superClass of [%s], Nothing to do -> STOP", superClass == null ? "null" : superClass.getName(), clazz.getName()));
                return Collections.emptyList();
            }
            CtMethod[] methods = clazz.getDeclaredMethods();
            CtMethod[] superMethods = superClass.getDeclaredMethods();
            Arrays.stream(superMethods).filter(t -> !isOverridenInClass(t, clazz)).forEach(ctMethods::add);
            ctMethods.addAll(getSuperClassesMethods(superClass));
            return ctMethods;
        }
        catch (Exception e) {
            logger.error(e);
            return Collections.emptyList();
        }
    }

    private static boolean hasKeyWords(CtClass clazz) {
        return indexOfAny(clazz.getName(), Config.getInstance().getKeywords()) > -1;
    }

    private static boolean hasSameSignature(CtMethod a, CtMethod b) {
        return a.getName().equals(b.getName()) && a.getSignature().equals(b.getSignature());
    }

    // todo unit test
    private static boolean isOverridenInClass(CtMethod method, CtClass clazz)  {
        return Arrays.stream(clazz.getDeclaredMethods()).anyMatch(classMethod -> hasSameSignature(method, classMethod));
    }

//    public static void main(String... args) throws NotFoundException {
//        ClassPool cp = ClassPool.getDefault();
//        // lib payapp
//        cp.insertClassPath("lib/*");
//        // search for this class
//        CtClass ctClass = cp.get("com.viettel.bankplus.payapp.manager.BusinessManager");
//        List<CtBehavior> methods = getUsedMembers(ctClass);
//        methods.forEach(t -> logger.debug(t.getLongName()));
//    }

    public static CtClass getMergeClass(String fullClassName, boolean recordError) {
        return _getClass(fullClassName, recordError, MERGE_DIR);

    }

    public static CtClass getLiveClass(String fullClassName, boolean recordError) {
        return _getClass(fullClassName, recordError, LIVE_DIR);

    }

    public static CtClass getUpcodeDir(String fullClassName, boolean recordError) {
       return _getClass(fullClassName, recordError, UPCODE_DIR);
    }


    public static CtClass _getClass(String fullClassName, boolean recordError, String classPath) {
        try {
            ClassPool cp = new ClassPool();
            cp.appendSystemPath();
            cp.insertClassPath(classPath);
            URL url = cp.find(fullClassName);
//            logger.info("CP: " + cp + ", hash code: " + cp.hashCode() + ", class loader: " + cp.getClassLoader());
            // search for this class
            CtClass clazz = cp.get(fullClassName);
//            logger.info(String.format("%s: %s, hashcode: %s ", fullClassName, url.getPath(), clazz.hashCode()));
            return clazz;
        }
        catch (Exception e) {
//            e.printStackTrace();
            if (recordError) {
                FinderObjectDependencyPool.getInstance().recordProblem("N/A", 0, ExceptionUtils.getRootCauseMessage(e), ExceptionUtils.getStackTrace(e));
            }
            return null;
        }
    }

    /**
     * @param expr
     */
    public static void reportWhere(Expr expr) {
        logger.debug(String.format("[%s] : Trigger fired at: [%s.%s]", expr.getEnclosingClass().getName(), expr.getEnclosingClass().getName(), expr.getLineNumber()));
    }

    public static String getRouteMessage(Exception e) {
        return e.getClass().getSimpleName() + ": " + e.getMessage();
    }

    public static List<String> listFiles(String dir) {
        String absDir = new File(dir).getAbsolutePath();
        String seperator = File.separator;
         List<String> files =  FileUtils.listFiles(new File(dir), new SuffixFileFilter(".class"), TrueFileFilter.INSTANCE).stream().map(file -> FilenameUtils.removeExtension(remove(file.getAbsolutePath(), absDir.endsWith(seperator) ? absDir : absDir + seperator).replace(seperator, "."))).collect(Collectors.toList());
       logger.info("list files: " + dir);
       files.forEach(logger::info);
       return files;
    }


    public static FinderComparator.CompareStatus compareMemberByteCodes(String className, String memberName, String desc, String classPathA, String classPathB) throws CannotCompileException {
        logger.info(String.format("[%s]: A [%s], B [%s], ", join(className, memberName, desc, '-'), classPathA, classPathB));
        CtClass classA = _getClass(className, true, classPathA);
        CtClass classB = _getClass(className, true, classPathB);
        if (classA == null && classB == null) {
            return A_NOT_EXIST_B_NOT_EXIST;
        }
        else if (classA == null) {
            return A_NOT_EXIST_B_EXIST;
        }
        else if (classB == null) {
            return A_EXIST_B_NOT_EXIST;
        }
        else {
            CtBehavior bevA = findBehaviours(classA, memberName, desc);
            CtBehavior bevB = findBehaviours(classB, memberName, desc);

            // todo
//            bevA.getMethodInfo2().

            if (bevA == null && bevB == null) {
                return A_NOT_EXIST_B_NOT_EXIST;
            }
            else if (bevA == null) {
                return A_NOT_EXIST_B_EXIST;
            }
            else if (bevB == null) {
                return A_EXIST_B_NOT_EXIST;
            }

            byte[] bytecodeA = bevA.getMethodInfo().getCodeAttribute().getCode();
            byte[] bytecodeB = bevB.getMethodInfo().getCodeAttribute().getCode();
            logger.debug("A: " + bevA.getName() + " : " + Arrays.toString(bytecodeA));
            logger.debug("B: " + bevB.getName() + " : " + Arrays.toString(bytecodeB));
            return Arrays.equals(bytecodeA, bytecodeB) ? A_B_COEXIST_SAME : A_B_COEXIST_DIFFERENT;
        }
    }

    static CtBehavior findBehaviours(CtClass clazz, String memberName, String desc) {
        CtBehavior[] entries = clazz.getDeclaredBehaviors();
//        Arrays.stream(entries).forEach(logger::debug);
        return Arrays.stream(entries).
                filter(ctBehavior -> ctBehavior.getName().equals(memberName) && ctBehavior.getSignature().equals(desc)).findFirst().orElse(null);
    }

    public static void main(String... args) throws NotFoundException, IOException, CannotCompileException {
        FinderComparators.compare("hi.MyClass", UPCODE_DIR, LIVE_DIR).forEach(logger::info);
    }
}
