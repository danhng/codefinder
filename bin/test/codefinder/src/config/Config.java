package config;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author thanhnd44 170508
 *         Viettel 2017 Confidential and Proprietary
 *         Created on 17/05/16
 */
public class Config {
    private boolean findKeyWordOnly;
    private String[] keywords;
    private int maxNestLevel;
    private Properties properties;
    private String template;

    public static final String MERGE_DIR = "../data/merge";
    public static final String LIVE_DIR = "../data/live";
    public static final String UPCODE_DIR = "../data/upcode";

    private Config() {}

    private static Config instance;

    private static Config parseConfiguration(String path) {
        Config config = new Config();
        config.properties = loadConfiguration();
        config.keywords = config.keywords();
        config.maxNestLevel = config.nestLevel();
        config.findKeyWordOnly = config.findKeyOnly();
        config.template = config.template();
        return config;
    }

    public String template() {
        return properties.getProperty("report_template");
    }

    public static Config getInstance() {
        if (instance == null) {
            instance = parseConfiguration("../etc/config.properties");
        }
        return instance;
    }

    public boolean isFindKeyWordOnly() {
        return findKeyWordOnly;
    }

    public void setFindKeyWordOnly(boolean findKeyWordOnly) {
        this.findKeyWordOnly = findKeyWordOnly;
    }

    public String[] getKeywords() {
        return keywords;
    }

    public void setKeywords(String[] keywords) {
        this.keywords = keywords;
    }

    public int getMaxNestLevel() {
        return maxNestLevel;
    }

    public void setMaxNestLevel(int maxNestLevel) {
        this.maxNestLevel = maxNestLevel;
    }

    private static Properties loadConfiguration() {
        Properties properties = new Properties();
        try (InputStream stream = new FileInputStream("../etc/config.properties")) {
            properties.load(stream);
//            properties.list(System.out);
            return properties;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private int nestLevel() {
        return Integer.parseInt(properties.getProperty("nest_level"));
    }

    private String[] keywords() {
        return properties.getProperty("keyword").split(",");
    }

    private boolean findKeyOnly() {
        return "1".equals(properties.getProperty("find_keyword_pkg_only"));
    }

    public class FileConfig {
        public static final String CONFIG_PARAM = "../etc/classes.properties";
    }
}
